package com.example.sourceit.sourceitexamplefirst;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by wenceslaus on 21.09.16.
 */
public class FirstActivity extends AppCompatActivity {

    //all views must be here
    private TextView label;
    private Button next;
    private Button updater;
    private EditText titleEdit;

    /**
     * First method, which run before UI was created
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //init UI
        setContentView(R.layout.activity);

        //find top label in layout
        label = (TextView) findViewById(R.id.title_label);
        //set text from resources
        label.setText(R.string.activity_one);

        //find button
        next = (Button) findViewById(R.id.next);
        //set listener to button
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //start next activity
                Intent intent = new Intent(FirstActivity.this, SecondActivity.class);
                startActivity(intent);
                //show small info
                Toast.makeText(FirstActivity.this, "new activity opened!", Toast.LENGTH_SHORT).show();
            }
        });

        //find top edit field in layout
        titleEdit = (EditText) findViewById(R.id.title_edit);

        //find updater button
        updater = (Button) findViewById(R.id.update);

        //set listener to button
        updater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get text from edit text field
                String newTitle = titleEdit.getText().toString();
                //set text to label
                label.setText(newTitle);
            }
        });
    }
}
