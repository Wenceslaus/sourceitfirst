package com.example.sourceit.sourceitexamplefirst;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by wenceslaus on 21.09.16.
 */
public class SecondActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity);

        TextView text = (TextView) findViewById(R.id.title_label);
        text.setText("activity last");

        //find button and make remove it from screen
        Button button = (Button) findViewById(R.id.next);
        button.setVisibility(View.INVISIBLE);

        //find button and set click listener
        Button updater = (Button) findViewById(R.id.update);
        updater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(SecondActivity.this, "don't work :(", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
